#ifndef SHOOT_H
# define SHOOT_H

# include <math.h>

# define WIDTH			1600
# define HEIGHT			800

# define PX				100
# define PY				100
# define AREA			30
# define ANGLE			10
# define PART_CREAT		10
# define PART_SIMULT	100
# define MAX_PART		200

# define COL(r, g, b)	((b) | ((g) << 8) | ((r) << 16))
# define PI				3.14159265359
# define TORAD(a)		((a) / 180.0 * PI)

typedef struct          s_vimg
{
    void                *ptr;
    char                *pxl;
    int                 bpp;
    int                 ln;
    int                 nd;
}                       t_vimg;

typedef struct			s_shoot
{
	double			vect[2];
	double			point[2];
	unsigned char	color[3];
	int				cycles;
	double			col_coef;
	double			vec_coef;
	struct s_shoot	*prev;
	struct s_shoot	*next;
}						t_shoot;

typedef struct			s_shoots
{
	unsigned char	color[3];
	double			min_area;
	double			max_area;
	int				angle;
	int				shoot_cycle;
	int				creat_shoots;
	int				max_shoots;
	int				nb_shoots;
	int				next_shoots;
	double			col_coef;
	double			vec_coef;

	int				count; // Non statiques
	int				countall;
	double			vect[2];
	int				center[2];
	t_shoot			*shoot;
	struct s_shoot	*last;
	struct s_shoots	*next;
}						t_shoots;

typedef struct		s_env
{
	void		*mlx;
	void		*win;
	t_vimg		img;
	t_shoots	*shoots;
}					t_env;


#endif /* !SHOOT_H */
