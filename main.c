#include <mlx.h>
#include <unistd.h>
#include "shoot.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

static int		ft_set_img(t_env *e, int x, int y, int color)
{
    unsigned int    col;
    int             *cpy;

    cpy = (int *)e->img.pxl;
	if (x > 0 && y > 0 && x < WIDTH && y < HEIGHT)
	{
		col = mlx_get_color_value(e->mlx, color);
		cpy[y * (e->img.ln / 4) + x] = col;
		return (1);
	}
	return (0);
}

/*
**					   |  AREA  |		 |	     SHOOTS        |   COEF   |
**			COLOR	    MIN MAX  SPRD CYC CREAT  MAX    ALL NEXT COL   VECT
*/
t_shoots	shoots_list(int i)
{
	static t_shoots	shoots[] =
	{
		{{0, 255, 30},    0,  7,   60,  7,   1, 1000,     1,  0, 0.80, 0.666},
		{{255, 25, 100},  0,  5,  360, 20,   1,   10,   100,  2,    1,    1},
		{{255, 255, 10},  0,  7,  360,  4,   2,    2,     2,  3,    1,  1},
		{{255, 255, 10},  0,  7,  360,  4,   1,   10,    10,  4,    1,  1},
		{{255, 255, 10},  0,  7,  360,  7,   1,   10,    10, -1,   0.4, 1},
	};
	return (shoots[i]);
}

void	blank_image(t_env *e)
{
	int		x;
	int		y;

	x = 0;
	while (x < WIDTH)
	{
		y = 0;
		while (y < HEIGHT)
		{
			ft_set_img(e, x, y, 0x000000);
			y++;
		}
		x++;
	}
}

int		shoot_mlx_init(t_env *e)
{
	if (!(e->mlx = mlx_init()))
		return (-1);
	if (!(e->win = mlx_new_window(e->mlx, WIDTH, HEIGHT, "Shoot")))
		return (-1);
	e->img.ptr = mlx_new_image(e->mlx, WIDTH, HEIGHT);
	e->img.pxl = mlx_get_data_addr(e->img.ptr, &e->img.bpp, &e->img.ln, &e->img.nd);
	return (0);
}

inline void		free_shoot(t_shoot **shoot)
{
	if ((*shoot)->prev)
		(*shoot)->prev->next = (*shoot)->next;
	if ((*shoot)->next)
		(*shoot)->next->prev = (*shoot)->prev;
	free(*shoot);
}

t_shoot		*shoot_init(t_shoot *prev, t_shoots *data)
{
	t_shoot		*new;
	double		len;
	double		angle;

	new = (t_shoot *)malloc(sizeof(t_shoot));
	if (prev)
		prev->next = new;
	new->next = NULL;
	new->prev = prev;
	len = (double)(rand() % 1000) / 1000 * (data->max_area - data->min_area);
	len += data->min_area;
	angle = TORAD((double)(rand() % (data->angle * 100)) / 100);
	new->col_coef = data->col_coef;
	new->vec_coef = data->vec_coef;
	new->vect[0] = cos(angle)*len + data->vect[0];
	new->vect[1] = sin(angle)*len + data->vect[1];
	new->point[0] = new->vect[0] + data->center[0];
	new->point[1] = new->vect[1] + data->center[1];
	new->color[0] = data->color[0];
	new->color[1] = data->color[1];
	new->color[2] = data->color[2];
	new->cycles = data->shoot_cycle;
	return (new);
}

t_shoots	*shoots_init(t_shoots *prev, t_shoots model, double *pos, double *vect)
{
	t_shoots	*new;

	new = (t_shoots *)malloc(sizeof(t_shoots));
	if (prev)
		prev->next = new;
	*new = model;
	new->count = 0;
	new->countall = 0;
	new->vect[0] = 0;
	new->vect[1] = 0;
	if (vect)
	{
		new->vect[0] = vect[0];
		new->vect[1] = vect[1];
	}
	new->center[0] = (int)pos[0];
	new->center[1] = (int)pos[1];
	new->color[0] = model.color[0];
	new->color[1] = model.color[1];
	new->color[2] = model.color[2];
	new->next = NULL;
	new->shoot = shoot_init(NULL, new);
	new->last = new->shoot;
	return (new);
}

void	shoot(t_env *e)
{
	int			color;
	t_shoots	*tmp;
	t_shoot		*tmp2;
	t_shoots	*last;

	tmp = e->shoots;
	while (tmp)
	{
		tmp2 = tmp->shoot->next;
		while (tmp2)
		{
			color = COL(tmp2->color[0], tmp2->color[1], tmp2->color[2]);
			if (!ft_set_img(e, (int)tmp2->point[0], (int)tmp2->point[1], color) && tmp->next_shoots == -1)
			{
				tmp2 = tmp2->next;
				free_shoot(&tmp2);
				continue ;
			}
			tmp2->point[0] += tmp2->vect[0];
			tmp2->point[1] += tmp2->vect[1];
			tmp2->vect[0] *= tmp2->vec_coef;
			tmp2->vect[1] *= tmp2->vec_coef;
			tmp2->color[0] *= tmp2->col_coef;
			tmp2->color[1] *= tmp2->col_coef;
			tmp2->color[2] *= tmp2->col_coef;
			(tmp2->cycles)--;
			if (tmp2->cycles <= 0)
			{
				if (tmp->next_shoots != -1)
				{
					last = tmp;
					while (last->next)
						last = last->next;
					shoots_init(last, shoots_list(tmp->next_shoots), tmp2->point, tmp2->vect);
				}
				tmp->count--;
				free_shoot(&tmp2);
			}
			tmp2 = tmp2->next;
		}
		tmp = tmp->next;
	}
	mlx_put_image_to_window(e->mlx, e->win, e->img.ptr, 0, 0);
	blank_image(e);
}

int		loop_hook(t_env *e)
{
	t_shoots	*tmp;
	t_shoot		*tofree;
	int			count;

	tmp = e->shoots;
	shoot(e);
	while (tmp)
	{
		count = 0;
		while (tmp->count < tmp->max_shoots &&
			   (tmp->nb_shoots == 0 || tmp->countall < tmp->nb_shoots)
			   && ++count <= tmp->creat_shoots)
		{
			tmp->count++;
			tmp->countall++;
			tmp->last = shoot_init(tmp->last, tmp);
		}
		count = 0;
		if (tmp->count > tmp->max_shoots)
		{
			while (count++ < tmp->creat_shoots)
			{
				tmp->count--;
				tofree = tmp->shoot;
				free(tofree);
				tmp->shoot = tmp->shoot->next;
			}
		}
		tmp = tmp->next;
	}
	return (0);
}

int		main(int ac, char **av)
{
	t_env		e;
	double		center[2] = {500, 400};
	double		vect[2] = {0.1, 0};

	srand(time(NULL));
	shoot_mlx_init(&e);
	e.shoots = shoots_init(NULL, shoots_list(1), center, vect);
//	usleep(20000);
	mlx_loop_hook(e.mlx, loop_hook, &e);
	mlx_loop(e.mlx);
	return (0);
}
