# README #

A particules generator.

Generates particules récursively.

Few colors available.

### Setup ###

Download, make and run.

Uses https://cdn.42.fr/elearning/INFOG-1-001/mlx-20140106.tgz

### Screenshots ###

![Screen Shot 2014-08-07 at 16.16.33.png](https://bitbucket.org/repo/ogojg6/images/1364784739-Screen%20Shot%202014-08-07%20at%2016.16.33.png)

![Screen Shot 2014-08-07 at 16.29.31.png](https://bitbucket.org/repo/ogojg6/images/609666626-Screen%20Shot%202014-08-07%20at%2016.29.31.png)

![Screen Shot 2014-08-07 at 16.34.17.png](https://bitbucket.org/repo/ogojg6/images/1852947563-Screen%20Shot%202014-08-07%20at%2016.34.17.png)

![Screen Shot 2014-08-07 at 16.40.00.png](https://bitbucket.org/repo/ogojg6/images/298903664-Screen%20Shot%202014-08-07%20at%2016.40.00.png)